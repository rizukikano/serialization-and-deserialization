﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    [Serializable()]
    class ObjectVehicle
    {
        public string Colour;
        public int Nopol;

        public ObjectVehicle(string _Color,int _Nopol)
        {
            Colour = _Color;
            Nopol = _Nopol;
        }

        public virtual void Spec()
        {

        }
    }

}
