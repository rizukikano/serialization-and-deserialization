﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    [Serializable()]
    class Bus : ObjectVehicle
    {
        public int TotalSeat;

        public Bus(string _Color, int _Nopol, int _TotalSeat) : base(_Color, _Nopol)
        {

            TotalSeat = _TotalSeat;
        }

        public override void Spec()
        {
            Console.WriteLine("Spesifikasi , Warna " + Colour + " , Nomor Polisi " + Nopol + " , Total Kursi " + TotalSeat);
        }


    }
}
