using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {


            Bus bus = new Bus("Hijau",3204,50);
            Truck truck = new Truck("Red", 4030, "Hino");

            PrintObject(bus);
            PrintObject(truck);
            

            Console.WriteLine("Serialization");

            byte[] busByte = Serialize(bus);
            byte[] truckByte = Serialize(truck);
            Console.WriteLine("Bus: ");
            PrintByte(busByte);

            Console.WriteLine("Truck: ");
            PrintByte(truckByte);

            Console.WriteLine(" Deserialization");

            // Deserialize player and enemy byte to object again
            ObjectVehicle Bus = Deserialize(busByte);
            ObjectVehicle Truck = Deserialize(truckByte);

            // Output same player and enemy object after deserialization
            PrintObject(Bus);
            PrintObject(Truck);

            Console.ReadKey();

        }

        static byte[] Serialize(Object _Vehicle)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, _Vehicle);
                return ms.ToArray();
            }
        }

        static ObjectVehicle Deserialize(byte[] _byteVehicle)
        {
            using (var memStream = new MemoryStream())
            {
                var binForm = new BinaryFormatter();
                memStream.Write(_byteVehicle, 0, _byteVehicle.Length);
                memStream.Seek(0, SeekOrigin.Begin);

                var obj = binForm.Deserialize(memStream);
                return (ObjectVehicle)obj;
            }

        }
        static void PrintObject(ObjectVehicle _objectVehicle)
        {
            _objectVehicle.Spec();
        }

        static void PrintByte(byte[] _objectVehicle)
        {
            Console.WriteLine("new byte[] { " + string.Join(", ", _objectVehicle) + " }");
        }


    }
}
