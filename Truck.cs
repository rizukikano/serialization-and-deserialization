﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    [Serializable()]
    class Truck : ObjectVehicle
    {
        public string Merk;

        public Truck(string _Color, int _Nopol , string _Merk) : base (_Color, _Nopol)
        {

            Merk = _Merk;
        }

        public override void Spec()
        {
            Console.WriteLine("Spesifikasi , Warna " + Colour + " , Nomor Polisi " + Nopol + " , Merk " + Merk);
        }
        
    }
}
